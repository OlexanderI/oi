import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';

import { User } from './models/user.model';

@Injectable()
export class AuthService {
  constructor(private http: HttpClient) {}

  signUp(user: User) {
    const body = JSON.stringify(user);
    const headers = new HttpHeaders({'Content-Type': 'application/json'});

    return this.http.post('http://localhost:3000/auth', body, {headers: headers})
      .map((response: HttpResponse<any>) => response)
      .catch((error: HttpResponse<any>) => {
        return Observable.throw(error);
      });
  }

  signIn(user: User) {
    const body = JSON.stringify(user);
    const headers = new HttpHeaders({'Content-Type': 'application/json'});

    return this.http.post('http://localhost:3000/auth/signin', body, {headers: headers})
      .map((response: HttpResponse<any>) => response)
      .catch((error: HttpResponse<any>) => {
        return Observable.throw(error);
      });
  }

  logout() {
    localStorage.clear();
  }

  isLoggedIn() {
    return localStorage.getItem('token') !== null;
  }

}
