import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router} from '@angular/router';

import { AuthService } from '../../auth.service';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  signup: FormGroup;

  constructor(private authService: AuthService, private router: Router) { }

  onRegisterSubmit() {
    // Create new instance of User
    const user = new User(
      this.signup.value.email,
      this.signup.value.password,
      this.signup.value.first_name,
      this.signup.value.last_name
    );
    this.authService.signUp(user)
      .subscribe(
        data => console.log(data),
        error => console.log(error)
      );
    this.signup.reset();
    this.router.navigateByUrl('/auth/login');
  }

  ngOnInit() {
    // Validate sign up form
    this.signup = new FormGroup({
      first_name: new FormControl(null, Validators.required),
      last_name: new FormControl(null, Validators.required),
      email: new FormControl(null, [
        Validators.required,
        Validators.pattern("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
      ]),
      password: new FormControl(null, Validators.required)
    });
  }

}
