import { Component, OnInit } from '@angular/core';
import { AuthService} from '../../modules/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) { }

  onLogout() {
    this.authService.logout();
    this.router.navigateByUrl('/auth/login');
  }

  isLoggedIn() {
    return this.authService.isLoggedIn();
  }

  ngOnInit() {
  }

}
